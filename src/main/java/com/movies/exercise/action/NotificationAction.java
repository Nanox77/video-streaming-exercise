package com.movies.exercise.action;

@FunctionalInterface
public interface NotificationAction {

	void send();

}