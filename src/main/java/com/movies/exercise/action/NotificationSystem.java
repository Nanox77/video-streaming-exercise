package com.movies.exercise.action;

import java.util.List;
import java.util.Map;

import com.movies.exercise.domain.User;
import com.movies.exercise.domain.Channel;
import com.movies.exercise.domain.UserSetting;
import com.movies.exercise.domain.UserSettingRepository;

public class NotificationSystem {

	private Map<Channel, NotificationAction> actionByChannel;
	private UserSettingRepository userSettingRepository;

	public NotificationSystem(UserSettingRepository userSettingRepository, Map<Channel, NotificationAction> actionByChannel) {
		this.userSettingRepository = userSettingRepository;
		this.actionByChannel = actionByChannel;
	}

	public void notify(User user) {
		UserSetting userSetting = this.userSettingRepository.getUserSetting(user.getId());
		List<Channel> channels = userSetting.getChannels();
		channels.forEach(channel -> {
			NotificationAction notificationAction = this.actionByChannel.getOrDefault(channel, new AbsentNotificationAction());
			notificationAction.send();
		});
	}
}