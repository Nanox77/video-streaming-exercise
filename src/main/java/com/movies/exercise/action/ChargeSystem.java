package com.movies.exercise.action;

import com.movies.exercise.domain.User;

public class ChargeSystem {

	private static final int CHARGES = 100;

	public int charge(User user) {
		user.charge(CHARGES);
		return user.getCredits();
	}
}