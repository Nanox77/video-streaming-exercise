package com.movies.exercise.action;

import java.util.List;

import com.movies.exercise.domain.Profile;
import com.movies.exercise.domain.User;
import com.movies.exercise.domain.UserProductRepository;
import com.movies.exercise.domain.InterestingProductEvaluation;
import com.movies.exercise.domain.OldProductEvaluation;
import com.movies.exercise.domain.Product;

public class RecommendationSystem {

	private UserProductRepository userProductRepository;

	public RecommendationSystem(UserProductRepository userProductRepository) {
		this.userProductRepository = userProductRepository;
	}

	public Profile evaluateIsOld(User user) {
		List<Product> products = userProductRepository.getProducts(user.getId());
		boolean isOld = products.stream().allMatch(product -> product.isOld(new OldProductEvaluation()));
		return isOld ? Profile.OLD : Profile.NONE;
	}

	public Profile evaluateIsInteresting(User user) {
		List<Product> products = userProductRepository.getProducts(user.getId());
		boolean isInteresting = products.stream().anyMatch(product -> product.isInteresting(new InterestingProductEvaluation()));
		return isInteresting ? Profile.INTERESTING : Profile.NONE;
	}
}