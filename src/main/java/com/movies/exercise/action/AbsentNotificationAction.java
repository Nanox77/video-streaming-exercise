package com.movies.exercise.action;

public class AbsentNotificationAction implements NotificationAction {

	@Override
	public void send() {
		System.out.println("ERROR: there is not notification action.");
	}
}