package com.movies.exercise.domain;

import java.util.Calendar;

public class OldProductEvaluation implements ProductEvaluation {

	@Override
	public boolean evaluate(Product product) {
		int year = Calendar.getInstance().get(Calendar.YEAR);
		return product.getReleaseYear() + 2 < year;
	}

}
