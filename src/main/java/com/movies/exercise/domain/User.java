package com.movies.exercise.domain;

import java.util.Objects;

public class User {

	private String id;
	private int credits;

	// TODO: Validate params.
	public User(String id) {
		this.id = id;
		this.credits = 1000;
	}

	public String getId() {
		return id;
	}

	public void charge(int charge) {
		int creditsWithCharge = credits - charge;
		if (creditsWithCharge < 0) {
			throw new RuntimeException("Credits not available");
		}
		this.credits -= charge;
	}

	public int getCredits() {
		return credits;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		User user = (User) o;
		return Objects.equals(id, user.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}