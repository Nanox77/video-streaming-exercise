package com.movies.exercise.domain;

import java.util.List;
import java.util.Objects;

public class UserSetting {

	private final String userId;
	private final List<Channel> channels;

	// TODO: Validate params.
	public UserSetting(String userId, List<Channel> channels) {
		this.userId = userId;
		this.channels = channels;
	}

	public String getUserId() {
		return userId;
	}

	public List<Channel> getChannels() {
		return channels;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		UserSetting that = (UserSetting) o;
		return Objects.equals(userId, that.userId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(userId);
	}
}