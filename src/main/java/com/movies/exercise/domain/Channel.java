package com.movies.exercise.domain;

public enum Channel {

	SMS, NOTIFICATION, MAIL

}