package com.movies.exercise.domain;

import java.util.Objects;

public class Movie implements Product {

	private String id;
	private float durationTime;
	private int releaseYear;
	private int oscarWon;

	// TODO: Validate params.
	public Movie(String id, float duration, int releaseYear, int oscarWon) {
		this.id = id;
		this.durationTime = duration;
		this.releaseYear = releaseYear;
		this.oscarWon = oscarWon;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public float getDurationTime() {
		return this.durationTime;
	}

	@Override
	public int getReleaseYear() {
		return this.releaseYear;
	}

	@Override
	public boolean isOld(OldProductEvaluation oldProductStrategy) {
		return oldProductStrategy.evaluate(this);
	}

	@Override
	public boolean isInteresting(InterestingProductEvaluation interestingProductStrategy) {
		return interestingProductStrategy.evaluateWhenIsMovie(this);
	}

	public boolean oscarWonAtLeast(int oscarsWon) {
		return this.oscarWon >= oscarsWon;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Series that = (Series) o;
		return Objects.equals(id, that.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}