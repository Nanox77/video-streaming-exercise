package com.movies.exercise.domain;

public interface UserSettingRepository {

	void store(UserSetting userSetting);

	UserSetting getUserSetting(String userId);

}