package com.movies.exercise.domain;

import java.util.Objects;

public class UserProduct {

	private final String userId;
	private final Product product;

	// TODO: Validate params.
	public UserProduct(String userId, Product product) {
		this.userId = userId;
		this.product = product;
	}

	public Product getProduct() {
		return product;
	}

	public String getUserId() {
		return userId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		UserProduct that = (UserProduct) o;
		return Objects.equals(userId, that.userId) && Objects.equals(product, that.product);
	}

	@Override
	public int hashCode() {
		return Objects.hash(userId, product);
	}
}