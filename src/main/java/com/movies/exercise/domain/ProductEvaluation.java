package com.movies.exercise.domain;

// Double dispatch implemented for different type of evaluations.
public interface ProductEvaluation {

	boolean evaluate(Product product);

}