package com.movies.exercise.domain;

public interface Product {

	String getId();

	float getDurationTime();

	int getReleaseYear();

	boolean isOld(OldProductEvaluation oldProductStrategy);

	boolean isInteresting(InterestingProductEvaluation interestingProductStrategy);
}