package com.movies.exercise.domain;

public class InterestingProductEvaluation implements ProductEvaluation {

	@Override
	public boolean evaluate(Product product) {
		return product.isInteresting(this);
	}

	public boolean evaluateWhenIsDocumentary(Documentary documentary) {
		return documentary.titleMatch("unofficial");
	}

	public boolean evaluateWhenIsMovie(Movie movie) {
		return movie.oscarWonAtLeast(1);
	}

	public boolean evaluateWhenIsSeries(Series series) {
		return series.hasSeason(4, 5);
	}
}