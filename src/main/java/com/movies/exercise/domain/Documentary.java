package com.movies.exercise.domain;

import java.util.Objects;

public class Documentary implements Product {

	private String id;
	private float durationTime;
	private int releaseYear;
	private String title;

	// TODO: Validate params.
	public Documentary(String id, float duration, int releaseYear, String title) {
		this.id = id;
		this.durationTime = duration;
		this.releaseYear = releaseYear;
		this.title = title;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public float getDurationTime() {
		return this.durationTime;
	}

	@Override
	public int getReleaseYear() {
		return this.releaseYear;
	}

	@Override
	public boolean isOld(OldProductEvaluation oldProductStrategy) {
		return oldProductStrategy.evaluate(this);
	}

	@Override
	public boolean isInteresting(InterestingProductEvaluation interestingProductStrategy) {
		return interestingProductStrategy.evaluateWhenIsDocumentary(this);
	}

	public boolean titleMatch(String word) {
		return this.title.contains(word);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Series that = (Series) o;
		return Objects.equals(id, that.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

}