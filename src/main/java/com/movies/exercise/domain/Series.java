package com.movies.exercise.domain;

import java.util.Objects;

public class Series implements Product {

	private String id;
	private float durationTime;
	private int releaseYear;
	private int season;

	// TODO: Validate params.
	public Series(String id, float duration, int releaseYear, int season) {
		this.id = id;
		this.durationTime = duration;
		this.releaseYear = releaseYear;
		this.season = season;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public float getDurationTime() {
		return this.durationTime;
	}

	@Override
	public int getReleaseYear() {
		return this.releaseYear;
	}

	@Override
	public boolean isOld(OldProductEvaluation oldProductStrategy) {
		return oldProductStrategy.evaluate(this);
	}

	@Override
	public boolean isInteresting(InterestingProductEvaluation interestingProductStrategy) {
		return interestingProductStrategy.evaluateWhenIsSeries(this);
	}

	public boolean hasSeason(int amountSeason, int anotherAmountSeason) {
		return this.season == amountSeason || this.season == anotherAmountSeason;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Series that = (Series) o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}