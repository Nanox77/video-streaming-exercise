package com.movies.exercise.domain;

import java.util.List;

public interface UserProductRepository {

	void store(UserProduct userProduct);

	List<Product> getProducts(String userId);

}