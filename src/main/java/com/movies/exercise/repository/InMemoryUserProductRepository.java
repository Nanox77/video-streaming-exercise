package com.movies.exercise.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.movies.exercise.domain.UserProduct;
import com.movies.exercise.domain.UserProductRepository;
import com.movies.exercise.domain.Product;

public class InMemoryUserProductRepository implements UserProductRepository {

	private Map<String, List<Product>> productsByUser;

	public InMemoryUserProductRepository() {
		this.productsByUser = new HashMap<>();
	}

	@Override
	public void store(UserProduct userProduct) {
		List<Product> products = getProducts(userProduct.getUserId());
		products.add(userProduct.getProduct());
		this.productsByUser.put(userProduct.getUserId(), products);
	}

	@Override
	public List<Product> getProducts(String userId) {
		return this.productsByUser.getOrDefault(userId, new ArrayList<>());
	}
}