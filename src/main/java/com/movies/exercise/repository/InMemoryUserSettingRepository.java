package com.movies.exercise.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.movies.exercise.domain.UserSettingRepository;
import com.movies.exercise.domain.UserSetting;

public class InMemoryUserSettingRepository implements UserSettingRepository {

	private Map<String, UserSetting> settingByUser;

	public InMemoryUserSettingRepository() {
		this.settingByUser = new HashMap<>();
	}

	@Override
	public void store(UserSetting userSetting) {
		this.settingByUser.put(userSetting.getUserId(), userSetting);
	}

	@Override
	public UserSetting getUserSetting(String userId) {
		return this.settingByUser.getOrDefault(userId, new UserSetting(userId, new ArrayList<>()));
	}

}