import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.movies.exercise.domain.Documentary;
import com.movies.exercise.repository.InMemoryUserProductRepository;
import com.movies.exercise.domain.Movie;
import com.movies.exercise.domain.Product;
import com.movies.exercise.domain.Profile;
import com.movies.exercise.action.RecommendationSystem;
import com.movies.exercise.domain.Series;
import com.movies.exercise.domain.User;
import com.movies.exercise.domain.UserProduct;
import com.movies.exercise.domain.UserProductRepository;

public class ClientInterestingProductTest {

	private Product interestingSeries;
	private Product interestingMovie;
	private Product interestingDocumentary;

	private Product nonInterestingSeries;
	private Product nonInterestingMovie;
	private Product nonInterestingDocumentary;

	@Before
	public void setUp() {
		interestingSeries = new Series(UUID.randomUUID().toString(), 1, 1992, 4);
		interestingMovie = new Movie(UUID.randomUUID().toString(), 2, 2017, 2);
		interestingDocumentary = new Documentary(UUID.randomUUID().toString(), 2, 2017, "unofficial drama");

		nonInterestingSeries = new Series(UUID.randomUUID().toString(), 1, 1992, 2);
		nonInterestingMovie = new Movie(UUID.randomUUID().toString(), 2, 2017, 0);
		nonInterestingDocumentary = new Documentary(UUID.randomUUID().toString(), 2, 2017, "Official drama");
	}

	@Test
	public void clientIsInterestingWhenWatchesSeriesWithFourOrMoreSeason() {
		User user = new User(UUID.randomUUID().toString());
		UserProductRepository userProductRepository = new InMemoryUserProductRepository();
		userProductRepository.store(new UserProduct(user.getId(), interestingSeries));

		RecommendationSystem recommendationSystem = new RecommendationSystem(userProductRepository);
		Profile profile = recommendationSystem.evaluateIsInteresting(user);

		Assert.assertEquals(profile, Profile.INTERESTING);
	}

	@Test
	public void clientIsInterestingWhenWatchesMoviesWithTwoOscarsWon() {
		User user = new User(UUID.randomUUID().toString());
		UserProductRepository userProductRepository = new InMemoryUserProductRepository();
		userProductRepository.store(new UserProduct(user.getId(), interestingMovie));

		RecommendationSystem recommendationSystem = new RecommendationSystem(userProductRepository);
		Profile profile = recommendationSystem.evaluateIsInteresting(user);

		Assert.assertEquals(profile, Profile.INTERESTING);
	}

	@Test
	public void clientIsInterestingWhenWatchesDocumentaryWithTitleContainsUnofficialWord() {
		User user = new User(UUID.randomUUID().toString());
		UserProductRepository userProductRepository = new InMemoryUserProductRepository();
		userProductRepository.store(new UserProduct(user.getId(), interestingDocumentary));

		RecommendationSystem recommendationSystem = new RecommendationSystem(userProductRepository);
		Profile profile = recommendationSystem.evaluateIsInteresting(user);

		Assert.assertEquals(profile, Profile.INTERESTING);
	}

	@Test
	public void clientIsNotInterestingWhenWatchesSeriesWithThreeSeason() {
		User user = new User(UUID.randomUUID().toString());
		UserProductRepository userProductRepository = new InMemoryUserProductRepository();
		userProductRepository.store(new UserProduct(user.getId(), nonInterestingSeries));

		RecommendationSystem recommendationSystem = new RecommendationSystem(userProductRepository);
		Profile profile = recommendationSystem.evaluateIsInteresting(user);

		Assert.assertEquals(profile, Profile.NONE);
	}

	@Test
	public void clientIsInterestingWhenWatchesMoviesWithZeroOscarsWon() {
		User user = new User(UUID.randomUUID().toString());
		UserProductRepository userProductRepository = new InMemoryUserProductRepository();
		userProductRepository.store(new UserProduct(user.getId(), nonInterestingMovie));

		RecommendationSystem recommendationSystem = new RecommendationSystem(userProductRepository);
		Profile profile = recommendationSystem.evaluateIsInteresting(user);

		Assert.assertEquals(profile, Profile.NONE);
	}

	@Test
	public void clientIsInterestingWhenWatchesDocumentaryWithoutTitleContainsUnofficialWord() {
		User user = new User(UUID.randomUUID().toString());
		UserProductRepository userProductRepository = new InMemoryUserProductRepository();
		userProductRepository.store(new UserProduct(user.getId(), nonInterestingDocumentary));

		RecommendationSystem recommendationSystem = new RecommendationSystem(userProductRepository);
		Profile profile = recommendationSystem.evaluateIsInteresting(user);

		Assert.assertEquals(profile, Profile.NONE);
	}

}