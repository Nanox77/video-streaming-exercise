import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.movies.exercise.repository.InMemoryUserProductRepository;
import com.movies.exercise.domain.Product;
import com.movies.exercise.domain.Profile;
import com.movies.exercise.action.RecommendationSystem;
import com.movies.exercise.domain.Series;
import com.movies.exercise.domain.User;
import com.movies.exercise.domain.UserProduct;
import com.movies.exercise.domain.UserProductRepository;

public class ClientOldTest {

	private Product product1;
	private Product product2;
	private Product product3;

	@Before
	public void setUp() {
		product1 = new Series(UUID.randomUUID().toString(), 1, 1990, 1);
		product2 = new Series(UUID.randomUUID().toString(), 2, 2005, 1);
		product3 = new Series(UUID.randomUUID().toString(), 3, 2017, 1);
	}

	@Test
	public void clientIsOldWhenOnlyWatchOldProducts() {
		User user = new User(UUID.randomUUID().toString());
		UserProductRepository userProductRepository = new InMemoryUserProductRepository();
		userProductRepository.store(new UserProduct(user.getId(), product1));
		userProductRepository.store(new UserProduct(user.getId(), product2));

		RecommendationSystem recommendationSystem = new RecommendationSystem(userProductRepository);
		Profile profile = recommendationSystem.evaluateIsOld(user);

		Assert.assertEquals(Profile.OLD, profile);
	}

	@Test
	public void clientIsNotOldWhenWatchNewsProducts() {
		User user = new User(UUID.randomUUID().toString());
		UserProductRepository userProductRepository = new InMemoryUserProductRepository();
		userProductRepository.store(new UserProduct(user.getId(), product1));
		userProductRepository.store(new UserProduct(user.getId(), product3));

		RecommendationSystem recommendationSystem = new RecommendationSystem(userProductRepository);
		Profile profile = recommendationSystem.evaluateIsOld(user);

		Assert.assertEquals(Profile.NONE, profile);
	}
}