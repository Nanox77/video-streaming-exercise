import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.movies.exercise.domain.Channel;
import com.movies.exercise.action.ChargeSystem;
import com.movies.exercise.repository.InMemoryUserSettingRepository;
import com.movies.exercise.action.NotificationAction;
import com.movies.exercise.action.NotificationSystem;
import com.movies.exercise.domain.User;
import com.movies.exercise.domain.UserSetting;
import com.movies.exercise.domain.UserSettingRepository;

public class ChargesTestSystem {

	private User user;
	private UserSettingRepository userSettingRepository;
	private NotificationSystem notificationSystem;
	private List<Channel> channels;
	private ChargeSystem chargeSystem;
	private int credits;

	@Before
	public void setUp() {
		user = new User(UUID.randomUUID().toString());
		userSettingRepository = new InMemoryUserSettingRepository();
		Map<Channel, NotificationAction> actions = createNotificationAction();
		notificationSystem = new NotificationSystem(userSettingRepository, actions);
		channels = new ArrayList<>();
		channels.add(Channel.SMS);
		chargeSystem = new ChargeSystem();
	}

	@Test
	public void whenUserPaySubscriptionThenSystemNotifyCharges() {
		givenUserSettingStored();
		whenCreditsAreCharges();

		notificationSystem.notify(user); // print "Send by sms"
		Assert.assertEquals(900, credits);
	}

	private void whenCreditsAreCharges() {
		credits = chargeSystem.charge(user);
	}

	private void givenUserSettingStored() {
		UserSetting userSetting = new UserSetting(user.getId(), channels);
		userSettingRepository.store(userSetting);
	}

	private Map<Channel, NotificationAction> createNotificationAction() {
		Map<Channel, NotificationAction> actions = new HashMap<>();
		actions.put(Channel.MAIL, () -> System.out.println("Send by mail"));
		actions.put(Channel.NOTIFICATION, () -> System.out.println("Send by notification"));
		actions.put(Channel.SMS, () -> System.out.println("Send by sms"));
		return actions;
	}

}